package Akshay.JavaProjects.bankingapp.controller;

import Akshay.JavaProjects.bankingapp.dto.AccountDTo;
import Akshay.JavaProjects.bankingapp.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }
    @PostMapping
    public ResponseEntity<AccountDTo> addAccount(@RequestBody AccountDTo accountDTo){
        return new ResponseEntity<>(accountService.createAccount(accountDTo), HttpStatus.CREATED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<AccountDTo> getAccountById(@PathVariable Long id){
   AccountDTo accountDTo = accountService.getAccountId(id);
   return ResponseEntity.ok(accountDTo);
    }
    @PutMapping("/{id}/deposit")
    public ResponseEntity<AccountDTo> deposit(@PathVariable  Long id, @RequestBody Map<String,Double> request){
        Double amount = request.get("amount");
       AccountDTo accountDTo = accountService.deposit(id,request.get("amount"));
       return ResponseEntity.ok(accountDTo);

    }
    @PutMapping("/{id}/withdraw")
    public ResponseEntity<AccountDTo> withdraw(@PathVariable  Long id,@RequestBody Map<String,Double> request){
        double amount = request.get("amount");
        AccountDTo accountDTo = accountService.withdraw(id,request.get("amount"));
        return ResponseEntity.ok(accountDTo);
    }
    @GetMapping
    public ResponseEntity<List<AccountDTo>> getAllAccount(){
        List<AccountDTo>accounts = accountService.getAllAccounts();
        return ResponseEntity.ok(accounts);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable Long id){
        accountService.deleteAccount(id);
        return ResponseEntity.ok("Account is deleted Sucessfully");
    }
}
